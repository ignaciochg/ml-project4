#!/usr/bin/env python3 

import numpy as np 
import matplotlib.pyplot as plt
import os

from pca import *

output_folder = "Output/"
output_type = "png"


def load_data(input_dir):
   files = os.listdir(input_dir)
   # out(files)
   if input_dir[-1] != "/":
      path = input_dir+"/"
   else:
      path = input_dir
   data = list()
   for file in files:
      pic  = np.array(plt.imread(path+file)).flatten()
      data.append(pic)

   data = np.array(data).astype(float).T
   return data

def convert_range(Input,InputLow, InputHigh,OutputLow,OutputHigh):
   return ((Input - InputLow) / (InputHigh - InputLow)) * (OutputHigh - OutputLow) + OutputLow;
def convertRanges(X, OutputLow, OutputHigh):
   InputLow = X.min()
   InputHigh = X.max()
   return convert_range(X,InputLow, InputHigh,OutputLow,OutputHigh)

def compress_images(DATA,k,centering=False, scaling=False):
   my_data = DATA.T
   numb_pics = my_data.shape[0]
   out("Numb Pics:", numb_pics)
   Z = compute_Z(my_data, centering, scaling)
   COV = compute_covariance_matrix(Z)
   egVal, egVec = find_pcs(COV)
   Z_star = project_data(Z,egVec,egVal,k, 0)
   out("initial projection:",Z_star.shape)
   
   X_comressed = project_data(Z_star,egVec,egVal,k,0, transposeU=True)
   out("initial compresion:",X_comressed.shape)

   X_comressed = convertRanges(X_comressed, 0, 255)


   temp = list()
   for pic in X_comressed:
      temp.append(pic.reshape((60, 48)))

   X_comressed = np.array(temp).astype(int)
   out("shaped compresion:",X_comressed.shape)

   # out(X_comressed.min(),X_comressed.max())

   # making folder structure
   try:
      os.mkdir(output_folder)
   except FileExistsError:
      pass

   variable = ""
   if centering:
      variable += "-Centered"
   if scaling:
      variable += "-Scaled-"
   else:
      variable += "-"

         # saving data
   for i in range(numb_pics):
      for pic in X_comressed:
         path = "{}K{}{}{}.{}".format(output_folder,k,variable,i,output_type)
         # out(X_comressed[i].shape)
         plt.imsave(path, X_comressed[i], vmin=0, vmax=255, cmap="gray")



if __name__ == "__main__":

   data_location = "Data/Train/"

   X = load_data(data_location)
   compress_images(X,100, centering=True, scaling=False)