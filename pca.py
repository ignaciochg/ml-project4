#!/usr/bin/env python3 


import numpy as np


DEBUG_MODE = False


def out(*argv):
   global DEBUG_MODE
   if DEBUG_MODE:
      m_string =""
      for arg in argv:
         m_string += str(arg)
      print(m_string)


def compute_Z(X, centering=True, scaling=False):
   current = X.astype(float)
   if centering:
      mean = np.mean(current,axis=0)
      current = current - mean
   if scaling:
      std = np.std(current,axis=0)
      current = current / std
   return current

def compute_covariance_matrix(Z):
   COV = (Z.T).dot(Z)
   # out(COV)
   return COV  # Z.T * Z

def toUnitLength(vector):
   length = np.linalg.norm(vector)
   v_hat = vector / length
   return v_hat

def find_pcs(COV):
   eigVal, eigVec = np.linalg.eig(COV)
   eigVal, eigVec = eigVal.real, eigVec.real
   eigVec = eigVec.T # because they come the other way

   sorted_eigVal_indexs = list(reversed(eigVal.argsort())) # biggest to smallest
   sorted_eigVal= eigVal[sorted_eigVal_indexs]
   sorted_eigVec = eigVec[sorted_eigVal_indexs]

   # make vectors unitlength
   temp = list()
   for vec in sorted_eigVec:
      temp.append(toUnitLength(vec))

   sorted_eigVec = np.array(temp)

   return sorted_eigVal, sorted_eigVec

def find_k(L,var):
   total = L.sum()

   for i in range(len(L)):
      calc = L[:i].sum() / total
      if calc >= var:
         return i

   return len(L)

def project_data(Z, PCS, L, k, var, transposeU=False):
   # out(Z)
   if k == 0:
      my_k = find_k(L, var)
   else:
      my_k = k
   # out(my_k)
   U = PCS[:my_k].T
   if transposeU:
      U = U.T
   # out(U)
   # out(U.T)
   # out(Z.shape, U.T.shape)
   projected = Z.dot(U)
   # out(projected)
   return projected

if __name__ == "__main__":

   X = np.array([[-1,-1],[-1,1],[1,-1],[1,1]])
   Z = compute_Z(X)
   COV = compute_covariance_matrix(Z)
   egVal, egVec = find_pcs(COV)
   k = 1
   Z_star = project_data(Z,egVec,egVal,k, 0)

   print("Z:")
   print(Z)
   print("COV:")
   print(COV)
   print("Eig vals:")
   print(egVal)
   print("Eig vecs:")
   print(egVec)
   print("K:")
   print(k)
   print("Z*:")
   print(Z_star)